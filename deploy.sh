#!/usr/bin/env bash


npm run clean && rollup -c --environment NODE_ENV:development
cd dist
scp -r * web.intra.wunderlin.net:/srv/www/wunderlin.net/svgmap
cd ..
