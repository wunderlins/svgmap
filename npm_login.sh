#!/usr/bin/env bash
REGISTRY="npm.pkg.github.com"
npm set @wunderlins:registry "https://$REGISTRY"
npm set "//$REGISTRY/:_authToken" "$NPM_CONFIG_TOKEN"
npm config set @wunderlins:_authToken "$NPM_CONFIG_TOKEN"
npm config set @wunderlins:username "wunderlins"
npm login --scope=@wunderlins --registry="https://$REGISTRY"
