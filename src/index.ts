import MapComponent from './map-component';

export function main() {
    //WebComponent.define();
    MapComponent.define();

    // wait for page
    let setValue = document.querySelector("form[id=mainForm] input[name=setValue") as HTMLInputElement;
    let btnSetValue = document.querySelector("#btnSetValue") as HTMLButtonElement;
    let mapSvg = document.querySelector("#mapSvg") as MapComponent;

    // register setValue event handler
    btnSetValue.addEventListener('click', () => {
        const map = document.querySelector('map-component[name="mapSelected"]') as MapComponent;
        map.highlight(setValue.value, true);
    });

    // register cmp1 change event handler
    mapSvg.addEventListener('change', (e: Event) => {
        let evt = e as CustomEvent;
        console.log(evt.detail);
        document.getElementById('cmp1Change')!.innerHTML = evt.detail.newValue;
        setValue.value = evt.detail.newValue;
    });
    
    // observe form changes
    const form = document.getElementById('mainForm') as HTMLFormElement;
    function dumpFormData() {
        const formData = new FormData(form);
        // declare a key value object
        const data: { [key: string]: any } = {};
        // iterate over the form data
        for (const [key, value] of formData.entries()) {
            data[key] = value;
        }
        console.log(data);
        const formContent = document.getElementById('formContent');
        formContent!.innerHTML = "<pre>" + JSON.stringify(data, null, 4) + "</pre>";
    }

    form.addEventListener('change', (e) => {
        e.preventDefault();
        dumpFormData();
    });
    dumpFormData();

    // highlight buttons
    document.querySelector("#button1")?.addEventListener("click", () => {
        mapSvg.highlight("stand_1", true);
    });

    document.querySelector("#button2")?.addEventListener("click", () => {
        mapSvg.highlight("stand_1", false);
    });
}

main();