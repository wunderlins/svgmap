import WebComponent from "./web-component";

export default class MapComponent extends WebComponent {
    public static override TAG_NAME = "map-component";
    public static override CLASS = MapComponent;
    public static override observedAttributes = ["src", "id", "name", "value"];

    private svg: SVGSVGElement | null | undefined = undefined;
    
    // this is the css id for the selectable objects
    // in the map. All these objects get an mouse over 
    // event handler attached
    private selectableId: string = "stand_"; // should be stand_1, stand_2, etc. whereis the number points to a datbase record

    protected override template = {
        html: `
            <object data="$src" type="image/svg+xml" id="hiddenImageLoader"></object>
        `,
        css: `
            #hiddenImageLoader { 
                width: 100%; 
                display: block;
            }
        `,
        map_css: `
            .highlight {
                stroke: red ! important;
                fill: red ! important;
            }
            .over {
                stroke: yellow ! important;
                fill: yellow ! important;
            }
        `
    }

    constructor() {
        super();

        // attach to the render event
        this.addEventListener("render", this.afterRender);
    }

    public highlight(id: string, add: boolean) {
        let lastValue = this.getAttribute("value");

        // if there is one highlighted already, unhighlight it
        if (lastValue) {
            this.svg?.getElementById(lastValue)?.classList.remove("highlight");
            if (lastValue === id) {
                // if the same element was clicked, then we are done
                this.setAttribute("value", "");
                return;
            }
        }
        
        // set the new value
        this.svg?.getElementById(id)?.classList.add("highlight");
        this.setAttribute("value", id);
    }

    private afterRender(ev: Event) {
        // query the shadow root for svg element and keep a reference
        let object = this.shadow.querySelector("#hiddenImageLoader") as HTMLObjectElement;
        object.addEventListener("load", () => {
            this.svg = object.contentDocument?.querySelector("svg");

            // Inject stylesheet into svg dom
            let style = document.createElement("style");
            style.innerHTML = this.template.map_css;
            this.svg?.appendChild(style);

            function mouseOverHandler(ev: MouseEvent) {
                let target = ev.target as SVGElement;
                console.log(target.id);
                target.classList.add("over");
            }

            function mouseOutHandler(ev: MouseEvent) {
                let target = ev.target as SVGElement;
                console.log(target.id);
                target.classList.remove("over");
            }

            this.svg?.querySelectorAll("[id*='"+this.selectableId+"']").forEach((el) => {
                let element = el as SVGElement;
                element.addEventListener("mouseover", mouseOverHandler);
                element.addEventListener("mouseout", mouseOutHandler);
                element.addEventListener("click", () => {
                    this.highlight(element.id, true);
                });
            });
        });

    }

    // do not re render when values change
    protected override render(): void {
        // render only once
        if (this.rendered) return;
        super.render();
    }
}