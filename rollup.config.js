// rollup.config.js
import static_files from '@wunderlins/rollup-plugin-static-files';
import typescript from '@rollup/plugin-typescript';
import { babel } from '@rollup/plugin-babel';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import strip from '@rollup/plugin-strip';
import terser from '@rollup/plugin-terser';
import InlineSvg from 'rollup-plugin-inline-svg';

let babelConfig = {
    babelHelpers: 'bundled',
    exclude: 'node_modules/**',
    include: ['src/**'],
    presets: [
        [
            '@babel/preset-env',
            {
                targets: {
                    ie: '11',
                },
            },
        ],
    ],
};

let rollupConfig = {
    input: "src/index.ts",
    output: {
        dir: './dist',
        format: 'iife',
        name: 'app',
        sourcemap: (process.env.NODE_ENV === 'production' ? false : true),
    },
    plugins: [
        static_files({
            include: ['./public'],
            publicPath: ''
        }),
        typescript(),
        InlineSvg(),
    ]
};

if (process.env.NODE_ENV === 'production') {
    rollupConfig.plugins = rollupConfig.plugins.concat([
        nodeResolve(),
        strip({
            include: 'src/**/*.(mjs|js|ts|tsx|jsx)',
            functions: ['console.log', 'console.error', 'console.assert', 'assert.*', 'debug'],
            debugger: true,
            sourceMap: true,
        }),
        babel(babelConfig),
        terser(),
    ]);
}

export default rollupConfig;
