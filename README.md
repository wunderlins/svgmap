# webcomp-ts

## Description

This is a web component library written in TypeScript. It is a work in progress.

## install 
    
```bash
npm install
```

## build

```bash
npm run build
```

## run

```bash
npm run serve
```

## References

- https://web.dev/articles/more-capable-form-controls
